# frozen_string_literal: true

module Google
  module Hangout
    module Webhook
      VERSION = '0.0.1'
    end
  end
end
