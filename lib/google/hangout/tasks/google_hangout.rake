namespace :google_hangout do
  desc 'Setup an example config yaml file for google hangout webhook'
  task :setup do
    puts ''
    puts 'Setting up google hangout webhooks...'
    puts ''
    config_file = Rails.root.join('config/google-hangout.yml').to_s
    initializer_file = Rails.root.join('config/initializers/google_hangout_webhook.rb').to_s

    if File.exist?(config_file)
      puts 'Config file already exist! Skipping'
    else
      puts 'Creating [config/google-hangout.yml] file.'
      File.open(config_file, 'w') do |file|
        file.write('---')
        file.write("\n")
        file.write('channels:')
        file.write("\n")
        file.write('  cheannel_name:')
        file.write("\n")
        file.write('    url: \'https://chat.googleapis.com/v1/spaces/...../messages?key=...&token=....\'')
        file.write("\n")
      end
    end

    if File.exist?(initializer_file)
      puts 'setup file already exist in initializers. Skipping'
    else
      puts 'Creating [config/initializers/google_hangout_webhook.rb] file.'
      File.open(initializer_file, 'w') do |file|
        file.write("# frozen_string_literal: true\n\n")
        file.write('GoogleHangout = Google::Hangout::Webhook::Message.new(\'config/google-hangout.yml\')')
        file.write('GoogleHangout.pre_message = [\'*\', Rails.env.upcase, "*\n"].join')
        file.write("\n\n")
        file.write("# GoogleHangout.broadcast(:example, '')\n")
        file.write("\n")
      end
    end

    puts ''
    puts 'Process complete.'
    puts ''
    true
  end
end
