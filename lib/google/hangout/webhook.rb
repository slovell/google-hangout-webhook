# frozen_string_literal: true

require 'google/hangout/webhook/version'
require 'google/hangout/railtie.rb' if defined?(Rails)

require 'rest-client'
require 'yaml'
require 'json'


module Google
  module Hangout
    module Webhook
      class Message
        attr_accessor :config_path, :settings, :pre_message, :response
        
        def initialize(config_path)
          self.config_path = config_path
          self.settings = YAML.load_file(config_path)
          self.pre_message = ''
        end

        def reload
          self.settings = YAML.load_file(config_path)
        end

        def broadcast(channel, message)
          raise "Channel name #{channel.to_s} not found" if settings['channels'][channel.to_s].nil?
          raise "Channel url is missing" if settings['channels'][channel.to_s]['url'].nil?

          self.response = RestClient.post(
            settings['channels'][channel.to_s]['url'],
            { text: [pre_message, message].join }.to_json,
            { content_type: :json, accept: :json }
          )
        rescue StandardError => e
          error = "Google Hangout Error: #{e.message}"
          puts error
          raise error
        end

      end
    end
  end
end
