# Google Hangout Webhook

This gem allows you to use google chat webhook to send messages.
You can only create or use webhook if you are in or have G Suite account.
G Suite account is not free for more information visit 
[GSuit](https://gsuite.google.com/)

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'google-hangout-webhook'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install google-hangout-webhook

## Configuration

You will have to create a webhook first.
To learn how to create a webhook please visit here
[Incoming webhooks](http://www.youtube.com/watch?v=ThKvlgnLyDk&t=3m38s).  
Once you have created a webhook take the **name** of the webhook and use it as
**channel** name.

If you are using Rails then run the rake task
`bundle exec rake google_hangout:setup` with will create all the necessary files
below.

Create a yml configuration file (if you have not used the rake task above) that
will have all the necessary data which you should put in your **.gitignore** file.
Lets assume you create a file called **google-hangout.yml** in **config** folder.
Now the file should look similar to this below:

```yml
---
channels:
  sync:
    url: 'https://chat.googleapis.com/v1/spaces/...../messages?key=...&token=....'

  import:
    url: 'https://chat.googleapis.com/v1/spaces/...../messages?key=...&token=....'

  export:
    url: 'https://chat.googleapis.com/v1/spaces/...../messages?key=...&token=....'
    
  product:
    url: 'https://chat.googleapis.com/v1/spaces/...../messages?key=...&token=....'
```

The channels are just a label for your convenience. They can be anything and
does not have to match the webhook name at all. All channel names must be lower
case (if possible) and should not have any spaces or special characters in them.
Think of it as you are declaring a variable. You can use any webhook at any
time using the channels.

## Rails

For Rails create a file in the **initializers** folder and put the following
code below.

*You may skip this setp if you ran the rake task "google_hangout:setup"*

```ruby
GoogleHangout = Google::Hangout::Webhook::Message.new('config/google-hangout.yml')
```

## Usage

First create an instance of the class and pass the location where you have your
hangout Configuration (as mentioned above). Then just broadcast your message
using a channel name.

```ruby
# Skip first line if you are using Rails and ran the rake task "google_hangout:setup"
GoogleHangout = Google::Hangout::Webhook::Message.new('config/google-hangout.yml')

GoogleHangout.broadcast(:sync, 'Something went wrong with sync ID....')
GoogleHangout.broadcast(:import, "Failed to import file...\n*Errors:*\n#{e.messages}")
GoogleHangout.broadcast(:export, 'Export timed out for report ID...')
GoogleHangout.broadcast(:export, ['Failed to save product :', @product.name, ' due to validation', "\nErrors:\n```", @product.error.messages.as_json, '```'].join)
```

The method **broadcast** takes two parameters.
First is the channel and second is the text message.
```ruby
GoogleHangout.broadcast(<channel>, <message>)
```

For more information regarding how you can style the text please visit:
[Simple Text Messages](https://developers.google.com/hangouts/chat/reference/message-formats/basic)


#### Pre-Message

If you want to setup a pre-message that will prepend to all message string then after creating
instance of the class just set the **pre_message** as shown below.

```ruby
GoogleHangout.pre_message = ['*', Rails.env.capitalize, "*\n"].join
```

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
